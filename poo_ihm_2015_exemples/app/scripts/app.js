'use strict';

/**
 * @ngdoc overview
 * @name pooIhmExemplesApp
 * @description
 * # pooIhmExemplesApp
 *
 * Main module of the application.
 */
angular
  .module('pooIhmExemplesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/404', {
        templateUrl: 'views/404.html',
        controller: 'AboutCtrl'
      })
      .when('/503', {
        templateUrl: 'views/503.html',
        controller: 'AboutCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      }) // users
      .when('/users' , {
        templateUrl: 'views/Users/list.html',
        controller: 'UsersCtrl'
      })
      .when('/users/add' , {
        templateUrl: 'views/Users/add.html',
        controller: 'UsersCtrl'
      })
      .when('/users/:userId/edit' , {
        templateUrl: 'views/Users/edit.html',
        controller: 'UsersCtrl'
      })
      .when('/users/:userId', {
        templateUrl: 'views/Users/show.html',
        controller: 'UsersCtrl'
      }) // roles
      .when('/roles' , {
        templateUrl: 'views/Roles/list.html',
        controller: 'RolesCtrl'
      })
      .when('/roles/add' , {
        templateUrl: 'views/Roles/add.html',
        controller: 'RolesCtrl'
      })
      .when('/roles/:roleId/edit' , {
        templateUrl: 'views/Roles/edit.html',
        controller: 'RolesCtrl'
      })
      .when('/roles/:roleId', {
        templateUrl: 'views/Roles/show.html',
        controller: 'RolesCtrl'
      }) // projects
      .when('/projects' , {
        templateUrl: 'views/Projects/list.html',
        controller: 'ProjectsCtrl'
      })
      .when('/projects/add' , {
        templateUrl: 'views/Projects/add.html',
        controller: 'ProjectsCtrl'
      })
      .when('/projects/:projectId/edit' , {
        templateUrl: 'views/Projects/edit.html',
        controller: 'ProjectsCtrl'
      })
      .when('/projects/:projectId', {
        templateUrl: 'views/Projects/show.html',
        controller: 'ProjectsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
