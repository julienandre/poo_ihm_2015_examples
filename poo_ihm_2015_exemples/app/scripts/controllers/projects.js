'use strict';

/**
 * @ngdoc function
 * @name pooIhmExemplesApp.controller:ProjectsCtrl
 * @description
 * # ProjectsCtrl
 * Controller of the pooIhmExemplesApp
 */
var app = angular.module('pooIhmExemplesApp')
  .controller('ProjectsCtrl', ['$scope', '$http', '$location', '$routeParams', function ($scope, $http, $location,$routeParams) {
    // init
    $scope.op = {"done":false,"status":"success","msg":""};

    // project list
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects')
      .success(function(data,status) {
        $scope.projects = data.data;
      });

    // project view
    if($routeParams.projectId) {
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/' + $routeParams.projectId)
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentProject = data.data;
            $scope.Title = data.data.title;
            $scope.Year = data.data.year;
            $scope.Descr = data.data.description;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/')
        .success(function(data) {
          if (data.status == "success") {
            $scope.users = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/' + $routeParams.projectId + '/Users')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentProject.users = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/' + $routeParams.projectId + '/Roles')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentProject.roles = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
    }

    $scope.addUser = function(name,userId,projectId){
      var json = {"name":name,"UserId":userId,"ProjectId":projectId};
      $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Roles/',json)
        .success(function(data){
          if (data.status == "success")
            roleAddSuccess();
          if (data.status == "error")
            roleAddFailure();
        })
        .error(function(data,status){
          roleAddFailure();
        })
    };

    var roleAddSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le rôle a bien été ajouté.";

      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/' + $routeParams.projectId + '/Users')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentProject.users = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/' + $routeParams.projectId + '/Roles')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentProject.roles = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
    };
    var roleAddFailure = function() {
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de l'ajout du rôle.";
    }

    // project add
    $scope.Year = 2015;

    $scope.add = function(id,title,year,description){
      var json = {"id":id,"title":title,"year":year,"description":description};
      $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/',json)
        .success(function(data){
          if (data.status == "success")
            projectAddSuccess();
          if (data.status == "error")
            projectAddFailure();
        })
        .error(function(data,status){
          projectAddFailure();
        })
    };

    var projectAddSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le projet a bien été ajouté.";
    };
    var projectAddFailure = function(){
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de l'ajout du projet.";
    };

    // project edit
    $scope.edit = function(id,title,year,description){
      var json = {"id":id,"title":title,"year":year,"description":description};
      $http.put('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+id,json)
        .success(function(data){
          if (data.status == "success")
            projectEditSuccess();
          if (data.status == "error")
            projectEditFailure();
        })
        .error(function(data,status){
          projectEditFailure();
        })
    };

    var projectEditSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le projet a bien été modifié.";
    };
    var projectEditFailure = function(){
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de la modification du projet.";
    };

    // project remove
    $scope.remove = function(id){
     $http.delete('http://poo-ihm-2015-rest.herokuapp.com/api/Projects/'+id)
      .success(function(data){
        if (data.status == "success")
          projectRemoveSuccess(id);
        if (data.status == "error")
          projectRemoveFailure(id);
      })
      .error(function(data,status){
        projectRemoveFailure(id);
      })
    }

    var projectRemoveSuccess = function(id){
      $("#project"+id).hide();
    };
    var projectRemoveFailure = function(id){
      $("#project"+id).addClass("alert-danger");
    };
  }]);

app.directive('numberOnlyInput', function () {
  return {
    restrict: 'EA',
    template: '<input class="form-control" name="{{inputName}}" ng-model="inputValue" />',
    scope: {
      inputValue: '=',
      inputName: '='
    },
    link: function (scope) {
      scope.$watch('inputValue', function(newValue,oldValue) {
        var arr = String(newValue).split("");
        if (arr.length === 0) return;
        if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
        if (arr.length === 2 && newValue === '-.') return;
        if (isNaN(newValue)) {
          scope.inputValue = oldValue;
        }
      });
    }
  };
});
