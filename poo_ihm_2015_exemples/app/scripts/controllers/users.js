'use strict';

/**
 * @ngdoc function
 * @name pooIhmExemplesApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the pooIhmExemplesApp
 */
angular.module('pooIhmExemplesApp')
  .controller('UsersCtrl', ['$scope', '$http', '$location', '$routeParams', function ($scope, $http, $location,$routeParams) {
    // user list
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users')
      .success(function(data,status) {
        $scope.users = data.data;
      });

    // user view
    if($routeParams.userId) {
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/' + $routeParams.userId)
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentUser = data.data;
            $scope.Name = data.data.name;
            $scope.Surname = data.data.surname;
            $scope.Email = data.data.email;
            $scope.Website = data.data.website;
          }
      })
        .error(function(data,status){
          $location.path("/503");
      });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/' + $routeParams.userId + '/Projects')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentUser.projects = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users/' + $routeParams.userId + '/Roles')
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentUser.roles = data.data;
          }
        })
        .error(function(data,status){
          $location.path("/503");
        });
    }

    // user add
    $scope.op = {"done":false,"status":"success","msg":""};

    $scope.add = function(name,surname,email,website){
      var json = {"name":name,"surname":surname,"email":email,"website":website};
      $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Users/',json)
        .success(function(data){
          if (data.status == "success")
            userAddSuccess();
          if (data.status == "error")
            userAddFailure();
        })
        .error(function(data,status){
          userAddFailure();
        })
    };

    var userAddSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "L'utilisateur a bien été ajouté.";
    };
    var userAddFailure = function(){
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de l'ajout de l'utilisateur.";
    };

    // user edit
    $scope.edit = function(id,name,surname,email,website){
      var json = {"id":id,"name":name,"surname":surname,"email":email,"website":website};
      $http.put('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+id,json)
        .success(function(data){
          if (data.status == "success")
            userEditSuccess();
          if (data.status == "error")
            userEditFailure();
        })
        .error(function(data,status){
          userEditFailure();
        })
    };

    var userEditSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le projet a bien été modifié.";
    };
    var userEditFailure = function(){
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de la modification du projet.";
    };

    // user remove
    $scope.remove = function(id){
      $http.delete('http://poo-ihm-2015-rest.herokuapp.com/api/Users/'+id)
        .success(function(data){
          if (data.status == "success")
            userRemoveSuccess(id);
          if (data.status == "error")
            userRemoveFailure(id);
        })
        .error(function(data,status){
          userRemoveFailure(id);
        })
    }

    var userRemoveSuccess = function(id){
      $("#user"+id).hide();
    };
    var userRemoveFailure = function(id){
      $("#user"+id).addClass("alert-danger");
    };
  }]);
