'use strict';

/**
 * @ngdoc function
 * @name pooIhmExemplesApp.controller:RolesCtrl
 * @description
 * # RolesCtrl
 * Controller of the pooIhmExemplesApp
 */
angular.module('pooIhmExemplesApp')
  .controller('RolesCtrl', ['$scope', '$http', '$location', '$routeParams', function ($scope, $http, $location,$routeParams) {
    // role list
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Roles')
      .success(function(data,status) {
        $scope.roles = data.data;
      });
    // user list
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Users')
      .success(function(data,status) {
        $scope.users = data.data;
      });
    // project list
    $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Projects')
      .success(function(data,status) {
        $scope.projects = data.data;
      });

    // role view
    if($routeParams.roleId) {
      $http.get('http://poo-ihm-2015-rest.herokuapp.com/api/Roles/' + $routeParams.roleId)
        .success(function(data) {
          if (data.status == "success") {
            $scope.currentRole = data.data;
            $scope.Name = data.data.name;
            $scope.UserSelect = data.data.UserId;
            $scope.ProjectSelect = data.data.ProjectId;
          }
      })
        .error(function(data,status){
          $location.path("/503");
      });
    }

    // role add
    $scope.op = {"done":false,"status":"success","msg":""};

    $scope.add = function(name,userId,projectId){
      var json = {"name":name,"UserId":userId,"ProjectId":projectId};
      $http.post('http://poo-ihm-2015-rest.herokuapp.com/api/Roles/',json)
        .success(function(data){
          if (data.status == "success")
            roleAddSuccess();
          if (data.status == "error")
            roleAddFailure();
        })
        .error(function(data,status){
          roleAddFailure();
        })
    };

    var roleAddSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le rôle a bien été ajouté.";
    };
    var roleAddFailure = function() {
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de l'ajout du rôle.";
    }

    // role edit
    $scope.edit = function(id,name,userId,projectId){
      var json = {"id":id,"name":name,"UserId":userId,"ProjectId":projectId};
      $http.put('http://poo-ihm-2015-rest.herokuapp.com/api/Roles/'+id,json)
        .success(function(data){
          if (data.status == "success")
            roleEditSuccess();
          if (data.status == "error")
            roleEditFailure();
        })
        .error(function(data,status){
          roleEditFailure();
        })
    };

    var roleEditSuccess = function(){
      $scope.op.done = true;
      $scope.op.status = "success";
      $scope.op.msg = "Le rôle a bien été modifié.";
    };
    var roleEditFailure = function(){
      $scope.op.done = true;
      $scope.op.status = "danger";
      $scope.op.msg = "Une erreur est survenue lors de la modification du rôle.";
    };

    // role remove
    $scope.remove = function(id){
      $http.delete('http://poo-ihm-2015-rest.herokuapp.com/api/Roles/'+id)
        .success(function(data){
          if (data.status == "success")
            roleRemoveSuccess(id);
          if (data.status == "error")
            roleRemoveFailure(id);
        })
        .error(function(data,status){
          roleRemoveFailure(id);
        })
    }

    var roleRemoveSuccess = function(id){
      $("#role"+id).hide();
    };
    var roleRemoveFailure = function(id){
      $("#role"+id).addClass("alert-danger");
    };
  }]);
